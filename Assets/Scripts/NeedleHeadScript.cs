﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeedleHeadScript : MonoBehaviour
{
   private void OnTriggerEnter2D(Collider2D other) 
   {
       if(other.CompareTag("needlehead"))
       {
           Debug.Log("Game End");
           Time.timeScale = 0f;
           ScoreManager.instance.ShowGameEndScreen();
       }
   }
}
