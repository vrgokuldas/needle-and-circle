﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;
   [SerializeField]
    private Text scoreText;
    private int score;
    [SerializeField]
    private Text resultScoreText;
    [SerializeField]
    private GameObject resultScreen;

    private void Awake() 
    {
        if(ScoreManager.instance == null)
        {
            ScoreManager.instance =this;
        }
        else if(ScoreManager.instance != this)
        {
            Destroy(this.gameObject);
        }
    }

    public void IncrementScore()
    {
        score+=1;
        SetScore();
    }

    private void SetScore()
    {
        scoreText.text= score.ToString();
    }

    public void ShowGameEndScreen()
    {
        resultScreen.SetActive(true);
        resultScoreText.text= score.ToString();
    }
}
