﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManagerScript : MonoBehaviour
{
   public static GameManagerScript instance;
   [SerializeField]
   private Button shootButton;
   [SerializeField]
   private GameObject needle;
   private GameObject[] needles;
   private float needleDistance=.5f;
   private int needleIndex;
   [SerializeField]
   private int howManyNeedles;

   private void Awake()
   {
        if(GameManagerScript.instance == null)
        {
            GameManagerScript.instance=this;
        }
        else if(GameManagerScript.instance != this){
            Destroy(this.gameObject);
        }
   }

   private void Start()
   {
    CreateNeedles();   
   }

   public void ShootNeedle()
   {
       needles[needleIndex].GetComponent<NeedleMovementScript>().FireTheNeedle();
       needleIndex+=1;
   }
   private void CreateNeedles()
   {
     needles= new GameObject[howManyNeedles];
     Vector3 temp=transform.position;
     for (int i = 0; i < howManyNeedles; i++)
     {
        needles[i] = Instantiate(needle,temp,Quaternion.identity) as GameObject;
        temp.y-=needleDistance; 
     }
   }

   public void InstantiateNeedle()
   {
       Instantiate(needle,transform.position,Quaternion.identity);
   }
   public void CheckGameEnd()
   {
       if(needleIndex == needles.Length)
       {
           Debug.Log("GAME FINISHED");
           Time.timeScale=0f;
           shootButton.gameObject.SetActive(false);
           ScoreManager.instance.ShowGameEndScreen();
       }
   }
}
