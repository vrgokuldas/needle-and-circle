﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleRotateScript : MonoBehaviour
{
   [SerializeField]
   private float rotationSpeed = 50f;
   private float angle;
   private bool canRotate =true;

   private void Start() 
   {
   StartCoroutine(ChangeCircleDirection());    
   }

   private void Update() {
       if(canRotate)
       {
           RotateTheCircle();
       }
   }

   private void RotateTheCircle()
   {
       angle=transform.rotation.eulerAngles.z;
       angle += rotationSpeed* Time.deltaTime;
       transform.rotation = Quaternion.Euler(new Vector3(0,0,angle));
   }

   IEnumerator ChangeCircleDirection()
   {
       yield return new WaitForSeconds(Random.Range(2,5));
       int direction=Random.Range(0,2);
       rotationSpeed = rotationSpeed * -1;
       if(direction == 0)
       {
            rotationSpeed= Random.Range(30,50);
       }
       else
       {
           rotationSpeed= Random.Range(60,110);
       }
       StartCoroutine(ChangeCircleDirection());
   }
}
