﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeedleMovementScript : MonoBehaviour
{
   [SerializeField] private GameObject needleBody;
   private bool canFireNeedle = false;
   private bool touchedTheCircle;
   [SerializeField]
   private float forceY = 150f;
   private Rigidbody2D myBody;

   private void Awake()
   {
   Initialize();
   }

   private void Initialize() 
   {
      needleBody.SetActive(false);
      myBody=GetComponent<Rigidbody2D>();
      myBody.isKinematic=true;
   }

   private void Update() 
   {
      if(canFireNeedle)
      {
         myBody.velocity=new Vector2(0,forceY*Time.deltaTime);
      }
      else
      {
          myBody.velocity=Vector2.zero;
      }
   }
   public void FireTheNeedle(){
      needleBody.SetActive(true);
      myBody.isKinematic=false;
      canFireNeedle=true;
   }

   private void OnTriggerEnter2D(Collider2D target) 
   {
      if(touchedTheCircle)
         return;
      if(target.CompareTag("Circle"))
      {
            Debug.LogFormat("<color=green>Touched the circle</color>");
            canFireNeedle=false;
            touchedTheCircle=true;
            myBody.isKinematic=true;
            transform.SetParent(target.transform);
            if(ScoreManager.instance != null)
            {
               ScoreManager.instance.IncrementScore();
            }
            GameManagerScript.instance.CheckGameEnd();
      }
   }
}
